from flask import Flask
from flask import request
from flask_cors import CORS, cross_origin
import os
import json
import torch
application = Flask(__name__)
cors = CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

model = None

@application.route('/message/<string:dialog_in>')
@cross_origin()
def dialog(dialog_in):
    if model is None:
        setup()
    dialog_out = model.respond(dialog_in)
    return dialog_out

@application.route('/', methods=['GET', 'POST'])
@cross_origin()
def communicate():
    if model is None:
        setup()
    print(request.data)
    data = request.data.decode('utf-8')
    data_json = json.loads(data)
    dialog_in = data_json.get("message")
    return model.respond(dialog_in)

def setup():
    global model
    print("Loading the AI...")
    path = get_model_path('model.pt')
    model = torch.load(path)
    print("Done.")

def get_model_path(name):
    directory = os.path.dirname(os.path.abspath(__file__))
    return directory + "/" + name

if __name__ == "__main__":
    setup()
    application.run()
