import torch
import torch.nn as nn
from torch import optim

from lang import SOS_TOKEN_IDX
from lang import EOS_TOKEN_IDX
from lang import SOS_TOKEN
from lang import EOS_TOKEN

import math
import time
import timer

from encoder import EncoderRNN
from decoder import AttentionDecoderRNN

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import random

MODEL_DIR = "../models/"

class AttnEncoderDecoder():
    def __init__(self, vocab, hidden_size, device, dropout_ratio=0.1, max_length=400, tfr=0.5, lr=0.001):
        self.vocab = vocab
        self.hidden_size = hidden_size
        self.max_length = max_length
        self.device = device
        self.tfr = tfr # teacher forcing ratio
        self.lr = lr # learning rate
        self.embedding = nn.Embedding(self.vocab.vocab_len, self.hidden_size)
        self.encoder = EncoderRNN(self.vocab.vocab_len, hidden_size, device)
        self.decoder = AttentionDecoderRNN(hidden_size, \
                                           self.vocab.vocab_len, \
                                           device, \
                                           dropout_ratio=dropout_ratio, \
                                           max_length=self.max_length)

    def train(self, train_data, n_iters, print_every=1000, plot_every=100, save_every=1000):
        start = timer.get_time()
        plot_losses = []
        print_loss_acc = 0
        plot_loss_acc = 0

        enc_optim = optim.Adam(list(self.encoder.parameters()) + list(self.embedding.parameters()), lr=self.lr)
        dec_optim = optim.Adam(list(self.decoder.parameters()) + list(self.embedding.parameters()), lr=self.lr)

        training_pairs = [self.vocab.tensors_from_pair(random.choice(train_data), self.device)
                          for i in range(n_iters)]
        criterion = nn.NLLLoss()

        for i in range(1, n_iters + 1):
            training_pair = training_pairs[i-1]
            input_tensor = training_pair[0]
            target_tensor = training_pair[1]
            loss = self.train_step(input_tensor, target_tensor, enc_optim, dec_optim, criterion)
            print_loss_acc += loss
            plot_loss_acc += loss

            if i % print_every == 0:
                print_loss_avg = print_loss_acc / print_every
                print_loss_acc = 0
                print('%s (%d %d%%) %.4f' % (timer.time_since(start, i/n_iters), i, i/n_iters * 100, print_loss_avg))
                rand_pair = random.choice(train_data)
                print(">> " + self.vocab.format_words(rand_pair[0].split()))
                print("== " + rand_pair[1])
                out = self.respond(rand_pair[0])
                print("<< " + out)
                print("")

            if i % plot_every == 0:
                plot_loss_avg = plot_loss_acc / plot_every
                plot_losses.append(plot_loss_avg)
                plot_loss_acc = 0

            if i % save_every == 0:
                path = compute_model_file_path(i)
                torch.save(self, path)
                print("(Saved checkpoint at " + path + ")")
                print("")

        display_plot_losses(plot_losses)

    def train_step(self, input_tensor, target_tensor, enc_optim, dec_optim, criterion):
        encoder_hidden = self.encoder.initHidden()

        enc_optim.zero_grad()
        dec_optim.zero_grad()

        input_length = input_tensor.size(0)
        target_length = target_tensor.size(0)

        encoder_outputs = torch.zeros(self.max_length, self.encoder.hidden_size, device=self.device)

        loss = 0

        for ei in range(input_length):
            encoder_output, encoder_hidden = self.encoder(input_tensor[ei], encoder_hidden, self.embedding)
            encoder_outputs[ei] = encoder_output[0, 0]

        decoder_input = torch.tensor([[SOS_TOKEN_IDX]], device=self.device)
        decoder_hidden = encoder_hidden

        teacher_forcing = True if random.random() < self.tfr else False
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attn = self.decoder(decoder_input, \
                                                                        decoder_hidden, \
                                                                        encoder_outputs, \
                                                                        self.embedding)
            if teacher_forcing:
                loss += criterion(decoder_output, target_tensor[di])
                decoder_input = target_tensor[di] # forcing next decoder step to look at ground truth
            else:
                topv, topi = decoder_output.topk(1)
                decoder_input = topi.squeeze().detach()
                loss += criterion(decoder_output, target_tensor[di])
                if decoder_input.item() == EOS_TOKEN_IDX:
                    break

        loss.backward()
        enc_optim.step()
        dec_optim.step()

        return loss.item() / target_length

    def evaluate(self, sentence):
        with torch.no_grad(): # no gradient updates during evaluation, beacause there is no target
            input_tensor = self.vocab.tensor_from_sentence(sentence, self.device)
            input_length = input_tensor.size()[0]
            encoder_hidden = self.encoder.initHidden()
            encoder_outputs = torch.zeros(self.max_length, self.encoder.hidden_size, device=self.device)

            for ei in range(input_length):
                encoder_output, encoder_hidden = self.encoder(input_tensor[ei], encoder_hidden, self.embedding)
                encoder_outputs[ei] += encoder_output[0,0]

            decoder_input = torch.tensor([[SOS_TOKEN_IDX]], device=self.device)
            decoder_hidden = encoder_hidden
            decoded_words = []
            decoder_attn = torch.zeros(self.max_length, self.max_length)

            for di in range(self.max_length):
                decoder_output, decoder_hidden, decoder_attn = self.decoder(decoder_input, \
                                                                            decoder_hidden, \
                                                                            encoder_outputs, \
                                                                            self.embedding)
                topv, topi = decoder_output.data.topk(1)
                if topi.item() == EOS_TOKEN_IDX:
                    decoded_words.append(EOS_TOKEN)
                    break
                else:
                    decoded_words.append(self.vocab.decoder[topi.item()])

                decoder_input = topi.squeeze().detach()

            return decoded_words

    def respond(self, sentence):
        return self.vocab.format_words(self.evaluate(sentence))

def display_plot_losses(plot_losses):
    plt.plot(plot_losses)
    plt.title("Loss per episode")
    plt.show()

def compute_model_file_path(epoch):
    t = str(math.floor(time.time())).zfill(16)
    return MODEL_DIR + t + "-model-epoch" + str(epoch) + ".pt"
