import torch
import torch.nn as nn

class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size, device):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.device = device

    def forward(self, x, hidden, embedding):
        embedded = embedding(x).view(1,1,-1)
        output = embedded
        return self.gru(output, hidden)

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=self.device)
