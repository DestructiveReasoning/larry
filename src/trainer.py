from lang import Vocabulary
from lang import normalize_string

import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as torchfun

from encoderdecoder import AttnEncoderDecoder

DATA_DIR = "../data/"
DATA_SRC_FILE = DATA_DIR + "x-from"
DATA_DST_FILE = DATA_DIR + "y-to"

def start_training():
    print("Loading data...")
    X, Y = load_data()
    print("Loaded {} data samples".format(len(X)))
    print("Initializing encoder-decoder model...")
    brain = AttnEncoderDecoder(vocab, 1000, device, lr=0.001, tfr=0.85)
    print("Done.")
    print("Beginning training...")
    brain.train(list(zip(X, Y)), 10000, print_every=20, save_every=500)

def load_data():
    X = []
    Y = []
    with open(DATA_SRC_FILE, 'r') as src:
        with open(DATA_DST_FILE, 'r') as dst:
            for (src_line, dst_line) in zip(src.readlines(), dst.readlines()):
                s = normalize_string(src_line)
                d = normalize_string(dst_line)
                if len(s) > 200 or len(d) > 200:
                    continue
                X.append(s)
                vocab.add_paragraph(s)
                Y.append(d)
                vocab.add_paragraph(d)

    assert len(X) == len(Y), "Error loading data: X and Y have different lengths ({} vs {})".format(len(X),len(Y))
    assert '' not in vocab.embedder, "ERROR: Empty string made its way into the vocabulary"
    return X, Y

if __name__ == "__main__":
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    vocab = Vocabulary()
    start_training()
