#!/bin/sh

DATA_DIR="../data/"

SRC="${DATA_DIR}x-from"
DST="${DATA_DIR}y-to"

if [[ $SRC ]]; then
	rm $SRC
fi
if [[ $DST ]]; then
	rm $DST
fi

cat "${DATA_DIR}"*"-src" > $SRC
cat "${DATA_DIR}"*"-dst" > $DST
