import re
import torch

SOS_TOKEN = "<SOS>"
EOS_TOKEN = "<EOS>"
UNK_TOKEN = "<UNK>"

SOS_TOKEN_IDX = 0
EOS_TOKEN_IDX = 1
UNK_TOKEN_IDX = 2

class Vocabulary:
    def __init__(self):
        self.embedder = {}
        self.counter = {}
        self.decoder = {SOS_TOKEN_IDX: SOS_TOKEN, EOS_TOKEN_IDX: EOS_TOKEN, UNK_TOKEN_IDX: UNK_TOKEN}
        self.vocab_len = 3
        self.empty_token_count = 0

    def add_token(self, token):
        if len(token) == 0:
            self.empty_token_count += 1
            print("Found empty token: %d" % self.empty_token_count)
            return
        if token not in self.embedder:
            self.embedder[token] = self.vocab_len
            self.decoder[self.vocab_len] = token
            self.vocab_len += 1
            self.counter[token] = 1
        else:
            self.counter[token] += 1

    def add_paragraph(self, paragraph):
        for token in paragraph.split():
            self.add_token(token)

    def get_embedding(self, token):
        if token not in self.embedder:
            return self.embedder[UNK_TOKEN_IDX]
        return self.embedder[token]

    def get_token(self, embedding):
        try:
            return self.decoder[embedding]
        except Exception as ex:
            print("WARNING: Somehow, get_token was called on an invalid embedding: {}".format(embedding))
            return UNK_TOKEN

    def indices_from_sentence(self, sentence):
        return [UNK_TOKEN_IDX if word not in self.embedder else self.embedder[word] for word in normalize_string(sentence).split()]

    def tensor_from_sentence(self, sentence, device):
        indices = self.indices_from_sentence(sentence)
        indices.append(EOS_TOKEN_IDX)
        return torch.tensor(indices, dtype=torch.long, device=device).view(-1,1)

    def tensors_from_pair(self, pair, device):
        input_tensor = self.tensor_from_sentence(pair[0], device)
        target_tensor = self.tensor_from_sentence(pair[1], device)
        return (input_tensor, target_tensor)

    def format_words(self, ws):
        words = list(map(lambda x: "I" if x == "i" else x, filter(lambda x: len(x) > 0, ws)))
        if len(words) == 0: return "<no response>"
        s = words[0] if len(words[0]) < 2 else words[0][0].upper() + words[0][1:]
        start_sentence = False
        for word in words[1:]:
            if is_contraction(word):
                s += "'{}".format(word)
            elif is_end_sentence(word):
                start_sentence = True
                s += word
                continue
            elif is_punctuation(word):
                s += word
            elif word == EOS_TOKEN:
                return s
            else:
                next_word = word if not start_sentence or len(word) < 2 else word[0].upper() + word[1:]
                s += " " + next_word
            start_sentence = False
        return s

def normalize_string(s):
    s = s.lower().strip()
    s = re.sub(r"([.!?,])", r" \1", s)
    s = re.sub(r"[^a-zA-Z0-9.!?,]", r" ", s)
    return s

def is_contraction(s):
    return True if s in ["s", "t", "d", "re", "ll", "m", "ve"] else False

def is_end_sentence(s):
    return True if s in [".","!","?"] else False

def is_punctuation(s):
    return True if s in [",", "'"] else False
