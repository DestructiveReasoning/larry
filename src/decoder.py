import torch
import torch.nn as nn
import torch.nn.functional as torchfun

class AttentionDecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, device, dropout_ratio=0.1, max_length=400):
        super(AttentionDecoderRNN, self).__init__()
        self.device = device
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.dropout_ratio = dropout_ratio
        self.max_length = max_length

        self.attention = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.max_length)
        self.dropout = nn.Dropout(self.dropout_ratio)
        self.gru = nn.GRU(self.max_length, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, x, hidden, enc_out, embedding):
        embedded = embedding(x).view(1,1,-1)
        embedded = self.dropout(embedded)

        attn_w = torchfun.softmax(self.attention(torch.cat((embedded[0], hidden[0]), 1)), dim=1)
        attn_applied = torch.bmm(attn_w.unsqueeze(0), enc_out.unsqueeze(0))

        output = torch.cat((embedded[0], attn_applied[0]), 1)
        output = self.attn_combine(output).unsqueeze(0)
        output = torchfun.relu(output)
        output, hidden = self.gru(output, hidden)
        output = torchfun.log_softmax(self.out(output[0]), dim=1)
        return output, hidden, attn_w

    def initHidden(self):
        return torch.zeros(1,1,self.hidden_size, device=self.device)
