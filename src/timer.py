import time
import math

def as_minutes(s):
    m = math.floor(s/60)
    s -= m*60
    return "{}m {}s".format(m,s)

def time_since(since, percent):
    now = time.time()
    s = now - since
    es = s / percent
    rs = es - s
    return "{} (- {})".format(as_minutes(s), as_minutes(rs))

def get_time():
    return time.time()
