import torch
import sys

def main():
    if len(sys.argv) < 2:
        print("USAGE: python " + sys.argv[0] + " <path to model file>")
        return

    path = sys.argv[1]
    model = torch.load(path)
    interact_with(model)

def interact_with(model):
    while(True):
        dialog_in = input('> ')
        if dialog_in == "exit":
            return
        dialog_out = model.respond(dialog_in)
        print(": " + dialog_out)

if __name__ == "__main__":
    main()
