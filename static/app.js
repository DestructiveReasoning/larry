window.onload = function() {
  document.getElementById("convo").value="";
  document.getElementById("convo-text").value="";
  document.getElementById("convo-in").onsubmit = function() {
    var box = document.getElementById("convo");
    var input = document.getElementById("convo-text");
    box.value += input.value + "\n";
    box.scrollTop = box.scrollHeight;

    var msg = {"message": input.value};
    input.value = "";

    $.ajax({
      method: "POST",
      url: "https://larry-chatbot.herokuapp.com",
      data: JSON.stringify(msg),
      contentType: "text/plain",
      dataType: "text",
      crossDomain: true,
    })
    .done(function(data) {
      box.value += "Larry responds: ";
      box.value += data + "\n";
      box.scrollTop = box.scrollHeight;
    });
    return false;
  }
}
