from scrapester import get_web_object
import re

SCRIPTS_BASE_URL = "https://springfieldspringfield.co.uk/"
SCRIPTS_INDEX = SCRIPTS_BASE_URL + "episode_scripts.php?tv-show="

class SpringFieldScraper():
    def __init__(self, tvshow):
        self.tvshow = tvshow
        self.quotes_src = "../data/{}-src".format(tvshow)
        self.quotes_dst = "../data/{}-dst".format(tvshow)

    def scrape(self):
        print("Accessing {} index at {}...".format(self.tvshow, SCRIPTS_INDEX + self.tvshow))
        obj = get_web_object(SCRIPTS_INDEX + self.tvshow)
        print("Searching for episodes...")
        links = get_episode_links(obj)
        print("Found {} episodes to scrape".format(len(links)))
        counter = 0
        for link in links:
            counter += 1
            print("{}/{}".format(counter, len(links)))
            link = re.sub("&amp;","&",link)
            print(SCRIPTS_BASE_URL + link)
            episode = get_web_object(SCRIPTS_BASE_URL + link)
            self.extract_data(str(episode.findAll("div", {'class': 'scrolling-script-container'})[0]))

    def extract_data(self, script):
        raw_lines = re.split("<br/?>", script)[1:-2]
        lines = clean_lines(raw_lines)
        with open(self.quotes_src, 'a') as src:
            with open(self.quotes_dst, 'a') as dst:
                for i in range(len(lines)-1):
                    src.write(lines[i] + "\n")
                    dst.write(lines[i + 1] + "\n")

def clean_lines(raw_lines):
    lines = []
    for i in range(len(raw_lines)):
        line = raw_lines[i]
        line = line.strip()
        if len(line) == 0:
            continue
        if line[0] == '-':
            line = line[1:]
            lines.append(line.strip())
        elif len(lines) > 0:
            lines[-1] += " " + line
        else:
            lines.append(line)
    return lines

def get_episode_links(index):
    hrefs = index.findAll("a", {'class': 'season-episode-title'})
    return [re.search('href="(.*)"', str(href)).group(1) for href in hrefs]

if __name__ == "__main__":
#    cyescraper = SpringFieldScraper("curb-your-enthusiasm")
#    cyescraper.scrape()
#    tpbscraper = SpringFieldScraper("trailer-park-boys")
#    tpbscraper.scrape()
    bojackscraper = SpringFieldScraper("bojack-horseman-2014")
    bojackscraper.scrape()
    officescraper = SpringFieldScraper("the-office-us")
    officescraper.scrape()
