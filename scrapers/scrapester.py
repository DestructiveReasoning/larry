from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from urllib.error import HTTPError

def get_web_object(url):
    try:
        return bs(urlopen(url), "lxml")
    except HTTPError:
        print("ERROR: Object at url " + url + " could not be downloaded.")
        return None
