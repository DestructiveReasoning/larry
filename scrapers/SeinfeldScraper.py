from scrapester import get_web_object
from itertools import dropwhile
import re

SEINFELD_SCRIPTS_BASE_URL = "http://www.seinfeldscripts.com/"
SEINFELD_INDEX = "seinfeld-scripts.html"

SEINFELD_QUOTES_SRC_FILE = "../data/seinfeld-src"
SEINFELD_QUOTES_DST_FILE = "../data/seinfeld-dst"

def scrape():
    links = get_episode_links()
    print("Found {} episode links to scrape".format(len(links)))
    for link in links:
        print("Scraping episode " + link[:-4])
        scrape_episode(link)

def scrape_episode(link_ext):
    obj = get_web_object(SEINFELD_SCRIPTS_BASE_URL + link_ext)
    lines = obj.findAll("p")
#    script = list(dropwhile(lambda x: x.get_text().lower().strip() != "quotes and scene summary:", lines))[2:]
    script = list(dropwhile(lambda x: re.search("^\[.*\]",x.get_text().strip()) is not None, lines))[2:]
    extract_data(map(lambda x: x.get_text(), script))

def extract_data(script):
    lines = []
    next_quote = False
    for line in script:
        line = re.sub(r"[\t\n]", " ", line)
        line = re.sub(r"\[.*\]", " ", line)
        line = re.sub(r"\(.*\)", " ", line)
        line = re.sub(r"  +", " ", line)
        if line.lower().strip() == '[end]' or line.lower().strip() == "end" or line.lower().strip() == "end of show.":
            break
        if line.count('-') > 10 or line.count('=') > 10 or line.count('*') > 10 or line.count('.') > 30:
            next_quote = True
            continue
        if len(line) > 200:
            next_quote = True
            continue
        if len(line.strip()) == 0 or line[0] == '%' or line[0] == '[':
            next_quote = True
            continue
        quote = re.search('^\w+:(.*)', line)
#        if quote is None:
#            quote = re.search('^[A-Z][A-Z][A-Z]+ (.*)', line)
        if quote is not None and len(quote.groups()) > 0:
            if len(quote.groups()[0].strip()) == 0:
                continue
            if len(lines) == 0 or len(lines[-1]) < 200:
                lines.append(quote.groups()[0].strip())
            else:
                lines[-1] = quote.groups()[0].strip()
        elif len(lines) == 0:
            continue
        elif not next_quote:
            lines[-1] = lines[-1].strip() + " " + line.strip()
        next_quote = False
    with open(SEINFELD_QUOTES_SRC_FILE, 'a') as src:
        with open(SEINFELD_QUOTES_DST_FILE, 'a') as dst:
            for i in range(len(lines)-1):
                src.write(lines[i] + "\n")
                dst.write(lines[i+1] + "\n")

def get_episode_links():
    obj = get_web_object(SEINFELD_SCRIPTS_BASE_URL + SEINFELD_INDEX)
    tables = obj.findAll("table")
    link_exts = []
    for table in tables[2:]:
        link_exts.extend(get_episodes_from_table(table))
    return link_exts

def get_episodes_from_table(table):
    return [re.search('href="(.*)"', str(link)).group(1).strip() for link in table.findAll("a")]

if __name__ == "__main__":
    scrape()
